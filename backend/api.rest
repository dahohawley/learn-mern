######## Users
POST http://localhost:5000/users/add HTTP/1.1
content-type: application/json

{
    "username" : "dahohawley3"
}

###

GET http://localhost:5000/users HTTP/1.1
content-type: application/json

####### Excercise Save
POST http://localhost:5000/excercises/add HTTP/1.1
Content-Type: application/json

{
    "username" : "dahohawley",
    "description" : "run",
    "duration" : 9,
    "date" : "2020-10-24T14:58:03.447Z"
}

### GET
GET http://localhost:5000/excercises HTTP/1.1

### GET BY ID
GET http://localhost:5000/excercises/5f944bc8ac43161fd881e27b HTTP/1.1
Content-Type: application/json

### Update 
POST http://localhost:5000/excercises/update/5f944bc8ac43161fd881e27b HTTP/1.1
Content-Type: application/json

{
    "username" : "dahohawley",
    "description" : "run edited!?!!2",
    "duration" : 20,
    "date" : "2020-10-24T14:58:03.447Z"
}

###
DELETE http://localhost:5000/excercises/5f944bc8ac43161fd881e27b HTTP/1.1
Content-Type: application/json
