const router = require('express').Router()
let User = require('../models/user.model');

router.route('/').get((req,res)=>{
    User.find()
        .then(users => res.json(users))
        .catch(err => res.status(400).json("error : " + err))
})

router.post('/add',(req,res)=>{
    const username = req.body.username
    
    const newUser   = new User({username});
    newUser.save()
        .then(() => res.json("Success ading new user"))
        .catch(err => res.status(400).json(`Error Creating User : ${err}`))
})

module.exports =  router;

