const { Router } = require('express');

const router = require('express').Router()
let Excercise = require('../models/excercise.model');

router.route('/').get((req,res)=>{
    Excercise.find()
        .then(excercises => res.json(excercises))
        .catch(err => res.status(400).json("error : " + err))
})

router.post('/add',(req,res)=>{
    const username = req.body.username;
    const description = req.body.description;
    const duration = Number(req.body.duration);
    const date = Date.parse(req.body.date)
    
    const newExcercise = new Excercise({
        username,
        description,
        duration,
        date
    });

    newExcercise.save()
        .then(()=>res.json("Excercise Added!"))
        .catch(err => res.status(400).json("error : " + err));
})

router.get("/:id",(req,res)=>{
    const id = req.params.id

    Excercise.findById(id)
        .then((excercise) => {
            res.json(excercise)
        })
        .catch((err)=>{
            res.status(400).json("Error : " +err )
        });
})

router.post("/update/:id",(req,res)=>{
    const id  = req.params.id;
    
    Excercise.findById(id).then((excercise) => {
        if(excercise){
            const postData = req.body;
            excercise.username = postData.username
            excercise.description = postData.description
            excercise.duration = Number(postData.duration)
            excercise.date  = Date.parse(postData.date)
            
            excercise.save()
                .then(()=>{
                    res.json("Success updating Excercise!")
                })
                .catch((error)=>{
                    res.status(400).json(`Error : ${error} `)
                })
        }else{
            res.status(400).json("No data found")
        }
    });
});

router.delete("/:id",(req,res) => {
    const id = req.params.id;
    Excercise.findByIdAndDelete(id).then(()=>{
        res.json("Excercise removed");        
    }).catch((err)=>{
        res.status(400).json(`Error : ${err}`)
    })
})
module.exports =  router;

