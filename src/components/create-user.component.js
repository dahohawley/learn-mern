import { Component } from "react";
import axios from 'axios'

export default class CreateUser extends Component{

    constructor(props){
        super(props)

        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onSubmit = this.onSubmit.bind(this)
        
        
        this.state = {
            username : ""
        }
    }

    onSubmit(e) {
        e.preventDefault();

        const user = {
            username: this.state.username,
        };

        axios.post("http://localhost:5000/users/add",user)
            .then((res)=>{
                console.log(res.data);
            })
            .catch((res)=>{
                console.error("Error!");
            })

        this.setState({
            username : ""
        })

    }

    onChangeUsername(e) {
        this.setState({
            username: e.target.value,
        });
    }

    render(){
        return (
            <div className="row">
                <div className="col-12">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label for="">username</label>
                            <input type="text" onChange={this.onChangeUsername} value={this.state.username} className="form-control"/>
                        </div>
                        <div class="form-group">
                            <button className="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}