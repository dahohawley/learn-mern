import Axios from "axios";
import { Component } from "react";
import {Link} from 'react-router-dom'


const Excercise = (props) => {
    return (
        <li className="list-group-item d-flex justify-content-between">
            <div className="detail">
                <p className="font-italic">{props.excercise.username}</p> 
                <p>{props.excercise.description}</p> 
                <p>{props.excercise.duration} Minutes</p> 
                <p>{props.excercise.date.substring(0,10)}</p> 

            </div>

            <div className="action">
                <div className="btn-group">
                    <button 
                        className="btn btn-sm btn-danger" 
                        onClick={()=>{
                            props.deleteExcercise(props.excercise._id)
                        }} >
                            Delete
                    </button>

                    <Link to={"/edit/"+props.excercise._id} className="btn btn-sm btn-info">Edit</Link>
                </div>
            </div>
        </li>
    )
}



export default class ExcercistList extends Component{
    constructor(props){
        super(props)
        this.deleteExcercise = this.deleteExcercise.bind(this)
        this.excerciseList  = this.excerciseList.bind(this)

        this.state = {excercises : []}
        
    }

    componentDidMount(){
        Axios.get("http://localhost:5000/excercises")
        .then((res)=>{
            if(res.status === 200 && res.data.length > 0){
                this.setState({
                    excercises : res.data
                })
            }
        })
    }

    deleteExcercise(id){
        Axios.delete("http://localhost:5000/excercises/"+id)
        .then((res)=>{
            console.log(res)
        })
        .catch(()=>{
            
        })

        this.setState({
            excercises : this.state.excercises.filter(el => el._id !== id)
        })
    }

    excerciseList(){
        return this.state.excercises.map((currentExcercise)=>{
            return <Excercise excercise={currentExcercise} deleteExcercise={this.deleteExcercise} key={currentExcercise._id} />
        });
    }

    render(){
        return (
            <div className="row">
                <div className="col-12">
                    <ul className="list-group">
                        {this.excerciseList()}
                    </ul>
                </div>
            </div>
        )
    }
}