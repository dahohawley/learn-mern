import Axios from "axios";
import { Component } from "react";
import Datepicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

export default class CreateExcercise extends Component {
  constructor(props) {
    super(props);

    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeDuration = this.onChangeDuration.bind(this);
    this.onChangeDate = this.onChangeDate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      username: "",
      description: "",
      duration: 0,
      date: new Date(),
      users: [],
    };
  }

  componentDidMount() {
    Axios.get("http://localhost:5000/users")
    .then((res)=>{
      if(res.status === 200 && res.data.length > 0){
        this.setState({
          users : res.data.map(user => user.username),
          username: res.data[0].username,
        });
      }
    })


  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value,
    });
  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value,
    });
  }

  onChangeDuration(e) {
    this.setState({
      duration: e.target.value,
    });
  }

  onChangeDate(date) {
    this.setState({
      date: date,
    });
  }

  onSubmit(e) {
    e.preventDefault();

    const excercise = {
      username: this.state.username,
      description: this.state.description,
      duration: this.state.duration,
      date: this.state.date,
    };

    Axios.post("http://localhost:5000/excercises/add",excercise)
      .then((res)=>{
        console.log(res);
      })
      .catch((err)=>{
        console.log(err)
      })

    console.log(excercise);

    window.location = "/";
  }

  render() {
    return (
      <div className="row">
        <div className="col-12">
          <h3>Create Excercise Log</h3>

          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label for="">Username</label>
              <select
                ref="userInput"
                className="form-control"
                required
                value={this.state.username}
                onChange={this.onChangeUsername}
              >
                  {
                    this.state.users.map(function(user){
                        return (
                          <option key={user} value={user}>
                              {user}
                          </option>     
                        )
                    })
                  }
              </select>
            </div>
            <div className="form-group">
              <label for="">Description</label>
              <input type="text" value={this.state.description} onChange={this.onChangeDescription} className="form-control" placeholder="" aria-describedby="helpId"/>
            </div>
            <div className="form-group">
              <label for="">Duration (inMinutes)</label>
              <input type="text" onChange={this.onChangeDuration} value={this.state.duration} className="form-control" />
            </div>
            <div className="form-group">
              <label for="">Date</label>
              <Datepicker
                className="form-control"
                onChange={this.onChangeDate}
                selected={this.state.date}
              />
            </div>
            <div class="form-group">
              <button type="submit" className="btn btn-primary">Create log</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
